\section{PARAMETER TYPES AND STRUCTURES}
\label{paratypes}

\subsection{Introduction}

Each parameter field in a TM or a TC packet has to hold a parameter value. To ensure the correct interpretation of a parameter value a parameter type is defined for each packet field. Through this type each parameter is associated with a defined set of possible values.\\
\newline
Only the parameters defined in this section shall be used when defining the TM or TC packets.

\subsection{Encoding Format}

Each parameter type and the corresponding encoding format are defined by a unique combination of Parameter Type Code (PTC) and Parameter Format Code (PFC).

\subsection{Overview PTCs}
\autoref{tab:PTCs} shows all PTCs defined in [STD-1]. Only the green coloured ones are used for the \textit{SeeSat}. 

\begin{table}[h]
\centering
	\begin{tabular}{p{0.1\textwidth}p{0.5\textwidth}}
	\toprule
	\textbf{PTC} & \textbf{Corresponding Parameter Type}\\
	\midrule
	\rowcolor{green!20}1 & Boolean\\
	\rowcolor{green!20}2 & Enumerated\\
	\rowcolor{green!20}3 & Unsigned Integer\\
	\rowcolor{green!20}4 & Signed Integer\\
	\rowcolor{green!20}5 & Real\\
	\rowcolor{green!20}6 & Bit-String\\
	7 & Octet-String\\
	8 & Character-String\\
	\rowcolor{green!20}9 & Absolute Time\\
	\rowcolor{green!20}10 & Relative Time\\
	\rowcolor{green!20}11 & Deduced\\
	\rowcolor{green!20}12 & Packet\\
	\bottomrule
	\end{tabular}
\caption{Overview PTCs [STD-1]}
\label{tab:PTCs}
\end{table}


\subsection{Parameter Type Definitions}


\subsubsection{Boolean}
The value PFC=1 is not used according to [STD-1]. To receive a length of one bit PFC=0 is used.\\
For \textit{SeeSat} a maximum length of 8 Bit for a \textit{Boolean} value is sufficient.
\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Value/Range}\\
	\hline
	\hline
	1&0&1 Bit& 0 = FALSE, \textgreater0 = TRUE\\
	\hline
	1&2\textless n\textless 8&n Bit&0 = FALSE, \textgreater0 = TRUE\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Boolean}
\end{table}


\subsubsection{Enumerated}

\begin{table}[H]
\centering
\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Value/Range}\\
	\hline
	\hline
	2&1\textless n \textless 64&n Bit& -\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Enumerated}
\end{table}


\subsubsection {Unsigned Integer}

\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Value/Range}\\
	\hline
	\hline
	3&0 to 12&(PFC+4)Bits& 0 to $2^{PFC+4} -1$\\
	\hline
	3&13&24 Bits& 0 to $2^{24} -1$\\
	\hline
	3&14&32 Bits& 0 to $2^{32} -1$\\
	\hline
	3&15&48 Bits& 0 to $2^{48} -1$\\
	\hline
	3&16&64 Bits& 0 to $2^{64} -1$\\
	\hline
	3&17&1 Bits& 0 to 1\\
	\hline
	3&18&2 Bits& 0 to 3\\
	\hline
	3&19&3 Bits& 0 to 7\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Unsigned Integer}
\end{table}


\subsubsection{Signed Integer}

\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Value/Range}\\
	\hline
	\hline
	4&0 to 12&(PFC+4)Bits& $-2^{PFC+3}$ to $2^{PFC+3} -1$\\
	\hline
	4&13&24 Bits& $-2^{23}$ to $2^{23} -1$\\
	\hline
	4&14&32 Bits& $-2^{31}$ to $2^{31} -1$\\
	\hline
	4&15&48 Bits& $-2^{47}$ to $2^{47} -1$\\
	\hline
	4&16&64 Bits& $-2^{63}$ to $2^{63} -1$\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Signed Integer}
\end{table}

\begin{description}
\item[Note 1]\hfill\\
Bit 0 of each \textit{Signed Integer} parameter determines the sign of the parameter value where $Bit\ 0 = 0$ denotes a positive value and $Bit\ 0 = 1$ denotes a negative value.
\end{description}


\subsubsection{Real}

\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Format}\\
	\hline
	\hline
	5&1&32 Bit& IEEE Simple Precision Format\\
	\hline
	5&2&64 Bit& IEEE Double Precision Format\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Real}
\end{table}
\newpage
\subsubsection{Bit-String}
For \textit{SeeSat} a maximum length of 64 Bit for a \textit{Bit-String} value is sufficient.

\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Format}\\
	\hline
	\hline
	6&0&variable& Variable Length Bit-String\\
	\hline
	6&0\textless n\textless64&n Bit& Fixed Length Bit-String\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Bit-String}
\end{table}
\begin{description}
\item[Note 1]\hfill\\
The meaning and interpretation of a bit-string value is application process specific.
\item[Note 2]\hfill\\
A variable-length bit-string consists of two fields: the \textit{length} field (type unsigned integer) which specifies the number of bits of the \textit{data} field and the \textit{data} field itself.\\
\end{description}

\subsubsection{Absolute Time}

The value of a \textit{Absolute Time} parameter field is a number of seconds and fractions of a second from a given epoch. It is a positive offset.

\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Format}\\
	\hline
	\hline
	9&1&48 Bits& 16 Bit day CDS format without a $\mu s$ field\\
	\hline
	9&2&64 Bits& 16 Bit day CDS format with a $\mu s$ field\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Absolute Time}
\end{table}

\begin{description}
\item[Note 1]\hfill\\
For the used CDS format the parameter field has the following structure:\\
day (16 Bit) | ms of day (32 Bit) |  $\mathrm{\mu s}$ of ms (16 Bit, only if PFC=2)
\end{description}

\subsubsection{Relative Time}

The value of a \textit{Relative Time} parameter field is a number of seconds and fractions of a second from the occurrence time of an event or a number of seconds and fractions of a second between two absolute times. It is a positive or negative offset.

\begin{table}[H]
\centering	\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Format}\\
	\hline
	\hline
	10&1&48 Bits& 16 Bit day CDS format without a $\mu s$ field\\
	\hline
	10&2&64 Bits& 16 Bit day CDS format with a $\mu s$ field\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Absolute Time}
\end{table}


\subsubsection{Deduced}

\begin{description}
\item[Note 1]\hfill\\
Each field has PFC = 0.
\item[Note 2]\hfill\\
The deduction shall only result from the content of one or more preceding fields of the same packet, of one or more mission constants or a combination of both.
\end{description}


\subsubsection{Packet}

\begin{table}[h]
\centering
\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.45\textwidth}|}
	\hline
	\textbf{PTC} & \textbf{PFC} & \textbf{Length} & \textbf{Format}\\
	\hline
	\hline
	12&0&maximum size of space packet& CCSDS telemetry packet compliant with [STD-2]\\
	\hline
	12&1&maximum size of space packet& CCSDS telecommand packet compliant with [STD-2]\\
	\hline
	\end{tabular}
\caption{Parameter Type Definition - Packet}
\end{table}