\section{TELECOMMAND STRUCTURE}

\subsection{Telecommand Packets}

All telecommand packets have to be conform to the structure defined in [STD-2] and shown below. They are transported using the communication protocol \textit{AX.25} which limits the maximum size of a \textit{Space Packet} to 256 Byte.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Pics/AufbauSpacePacket.eps}
\caption{Structure of TC Space Packet according to [STD-2]}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Pics/AufbauPacketPrimaryHeader.eps}
\caption{Structure of TC Space Packet Primary Header according to [STD-2]}
\end{figure}

\subsubsection{Packet Header}

This section describes all elements of the \textit{Space Packet Primary Header}.

\begin{description}
\item[Packet Version Number]\hfill\\
Identifies the packet as a \textit{Space Packet}. Must be set to zero ('000').
\item[Packet Type]\hfill\\
Indicates whether the \textit{Space Packet} is a TM or a TC packet. For TC packets it must be set to '1'.
\item[Secondary Header Flag]\hfill\\
Indicates the presence of a \textit{Packet Secondary Header} when set to '1'.
\item[Application Process Identifier]\hfill\\
The APID uniquely identifies the destination of the TC packet.
\item[Sequence Flags]\hfill\\
Indicates whether the transmitted segment is the first, a middle or the last one if a segmentation is present. All PUS packets are defined as \textit{not segmented}, so the value must be set to '11' according to [STD-1].
\item[Packet Sequence Count or Packet Name]\hfill\\
Identifies the segment through a counter value or through a name.
\item[Packet Data Length]\hfill\\
Specifies the number of octets contained in the \textit{Packet Data Field}. The length of the complete \textit{Space Packet} will be implicitly 6 octets longer, because of the standardized 6 octets long header needed for all packets.\\ This number is defined as an unsigned integer C where\\
C = (Number of octets in \textit{Packet Data Field}) - 1
\end{description}


\subsubsection{Packet Data Field}

The \textit{Packet Data Field} consists of the \textit{Packet Secondary Header} and the \textit{User Data Field}.
The TC \textbf{Packet Secondary Header} is specified in [STD-1] and shown in \autoref{img:TCSecondary}. The TC \textit{Packet Secondary Header} is mandatory for all TC packets except for the CPDU command packet (TC(2,4)).

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Pics/AufbauSecondaryHeaderTC.eps}
\caption{Structure of TC Packet Secondary Header according to [STD-1]}
\label{img:TCSecondary}
\end{figure}

\begin{description}
\item[TC Packet PUS Version Number]\hfill\\
This field reflects the different versions of the PUS Standard. Must be set to '0010' to correspond with ECSS-E-70-41C ([STD-1]).
\item[Acknowledgement Flags]\hfill\\
This field indicates which acknowledgements are required. These are transmitted in form of TMs of Service 1. The TMs which indicate a failure are always transmitted. This field only specifies whether or not the successfully reports should be transmitted, too. 
\begin{itemize} 
\item Bit 3 (x x x 1): If bit 3 is set, TM(1,1) (successful acceptance) is required.
\item Bit 2 (x x 1 x): If bit 2 is set, TM(1,3) (successful start of execution) is required.
\item Bit 1 (x 1 x x): If bit 1 is set, TM(1,5) (successful progress of execution) is required.
\item Bit 0 (1 x x x): If bit 0 is set, TM(1,7) (successful completion of execution) is required.
\end{itemize}
\item[Message Type ID]\hfill\\
This field contains the defined Service Type and Service Subtype of the TC.
\item[Source ID]\hfill\\
This field contains the source identifier of the TC corresponding with the \textit{Application Process User Identifier} of the generating process.
\item[Spare]\hfill\\
Optional field. Can be used to constrain the length of the TC Packet Secondary Header to an integral number of words. The \textit{Spare} field is not used in this project.
\end{description}

The \textbf{User Data Field} contains the application data which is specific for each message type ID. These fields are specified in \autoref{sec:services}. It also contains a 16 Bit long \textit{Packet Error Control Checksum} (ISO or CRC) to verify the integrity of the TC packet.

\subsubsection{Allowed lengths of TC packets}
\subsubsection*{Maximum allowed length of telecommand packets}

If time- and/or position-based scheduling is used it is necessary to add TCs into the schedule. To render this possible a complete TC (so called \textit{Request}) has to fit into the data field of another TC of type TC(11,4) (\textit{Insert activities into the time-based schedule}) or of type TC(22,4) (\textit{Insert activities into the position-based schedule}). TC(22,4) limits the maximum length of a TC packet, because its application data field contains the most data additional to the TC to insert (11 Byte). If any TC will exceed the maximum length it is not possible to schedule this command. \\
The maximum allowed length of the application data field of a TC is calculated as follows:\\
\textit{max length of application data field of TC = max length of packet - length of Packet Primary Header - length of Packet Secondary Header - length of Checksum = 256 Byte - 6 Byte - 5 Byte - 2 Byte = 243 Byte}\\
This calculation is valid if the \textit{Spare} field in the \textit{Packet Secondary Header} is not used. \\
The calculated value above represents the possible length of, for example, TC(22,4). Each TC which should be scheduled using TC(22,4) has a maximum length calculated as follows:\\
\textit{max length of scheduled TC = max length of application data field of TC - length of additional data in TC(22,4) = 243 Byte - 11 Byte = 232 Byte}\\
The maximum length of the application data of a scheduled TC is calculated as follows:\\
\textit{max length of application data of scheduled TC = max length of scheduled TC - length of Packet Primary Header - length of Packet Secondary Header - length of Checksum = 232 Byte - 6 Byte - 5 Byte - 2 Byte = 219 Byte}\\
This calculation is valid if the \textit{Spare} field in the \textit{Packet Secondary Header} is not used.

\subsubsection*{Minimum length of telecommand packets}

The minimum length of a telecommand packet is calculated as follows:\\
\textit{min length of TC packet = length of Packet Primary Header + length of Packet Secondary Header + length of Checksum = 6 Byte + 5 Byte + 2 Byte = 13 Byte}\\
This calculation is valid if the \textit{Spare} field in the \textit{Packet Secondary Header} is not used.


\newpage
\section{TELEMETRY STRUCTURE}

\subsection{Telemetry Packet}

All telemetry packets have to be conform to the same structure as TCs, which is defined in [STD-2] and shown below. They are transported using the communication protocol \textit{AX.25} which limits the maximum size of a \textit{Space Packet} to 256 Byte.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Pics/AufbauSpacePacket.eps}
\caption{Structure of TM Space Packet according to [STD-2]}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Pics/AufbauPacketPrimaryHeader.eps}
\caption{Structure of TM Space Packet Primary Header according to [STD-2]}
\end{figure}


\subsubsection{Packet Header}

This section describes all elements of the \textit{Space Packet Primary Header}.

\begin{description}
\item[Packet Version Number]\hfill\\
Identifies the Packet as a \textit{Space Packet}. Must be set to zero ('000').
\item[Packet Type]\hfill\\
Indicates whether the \textit{Space Packet} is a TM or a TC packet. For TM packets it must be set to '0'.
\item[Secondary Header Flag]\hfill\\
Indicates the presence of a \textit{Packet Secondary Header} when set to '1'.
\item[Application Process Identifier]\hfill\\
The APID uniquely identifies the source of the TM packet.
\item[Sequence Flags]\hfill\\
Indicates whether the transmitted segment is the first, a middle or the last one if a segmentation is present. All PUS packets are defined as \textit{not segmented}, so the value must be set to '11' according to [STD-1].
\item[Packet Sequence Count or Packet Name]\hfill\\
Identifies the segment through a counter value or through a name.
\item[Packet Data Length]\hfill\\
Specifies the number of octets contained in the \textit{Packet Data Field}. The length of the complete \textit{Space Packet} will be implicitly 6 octets longer, because of the standardized 6 octets long header needed for all packets.\\ This number is defined as an unsigned integer C where\\
C = (Number of octets in \textit{Packet Data Field}) - 1
\end{description}

\subsubsection{Packet Data Field}

The \textit{Packet Data Field} consists of the \textit{Packet Secondary Header}, if present, and the \textit{User Data Field}.
\newline
The TM \textbf{Packet Secondary Header} is specified in [STD-1] and shown in \autoref{fig:TMSecondary}. The TM Secondary Header is mandatory for all TM packets except for the time reports (TM(9,2) and TM(9,3)).

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Pics/AufbauSecondaryHeaderTM.eps}
\caption{Structure of TM Packet Secondary Header according to [STD-1]}
\label{fig:TMSecondary}
\end{figure}

\begin{description}
\item[TM Packet PUS Version Number]\hfill\\
This field reflects the different versions of the PUS Standard. Must be set to '0010' to correspond with ECSS-E-70-41C ([STD-1]).
\item[Spacecraft Time Reference Status]\hfill\\
This field contains the status of the on board time reference used when time tagging the TM packet. If the capability to report this status is not provided the field has to be set to 0. 
\item[Message Type ID]\hfill\\
This field contains the defined Service Type and Service Subtype of the TM.
\item[Message Type Counter]\hfill\\
This field contains the count of messages per destination if the application process provides the capability to count them. If this capability is not provided the field has to be set to 0.
\item[Destination ID]\hfill\\
This field contains the destination identifier corresponding with the APID of the process addressed by the report.
\item[Time]\hfill\\
This field contains the time tag of the report which is transmitted by this TM packet. The size of this field is 64 Bit (8 Byte).
\item[Spare]\hfill\\
Optional field. Can be used to constrain the length of the TM Packet Secondary Header to an integral number of words. The \textit{Spare} field is not used in this project.
\end{description}

The \textbf{User Data Field} contains the source data which is specific for each message type ID. These fields are specified in \autoref{sec:services}. It also contains a 16 Bit long \textit{Packet Error Control Checksum} (ISO or CRC) to verify the integrity of the TM packet.

\subsubsection{Allowed lengths of TM packets}
\subsubsection*{Maximum allowed length of telemetry packets}

The maximum allowed length of the source data of a telemetry message is calculated as follows:\\
\textit{max length of source data of a TM = max length of packet - length of Packet Primary Header - length of Packet Secondary Header - length of Checksum = 256 Byte - 6 Byte - 15 Byte - 2 Byte = 233 Byte}\\
This calculation is valid if the \textit{Spare} field in the \textit{Packet Secondary Header} is not used.


\subsection{Direct Telemetry Packets}

A direct TM service provided on board allows data like housekeeping data to be transmitted to ground without any software involvement.

\subsection{Critical TM}

Critical TM packets are the packets which will still be transmitted to ground and saved in the TM buffer on board if there is a bad connection to ground including a over-subscribed bandwidth. The decision whether a TM packet is critical or not is made based on APID, type, subtype and SID (Structure Identifier).